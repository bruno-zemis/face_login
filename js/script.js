
window.onload  = function () {
    const client_id = 472533196581475
    const redirect_uri = 'https://localhost:8080/login'
    const url = `http://www.facebook.com/v3.1/dialog/oauth?client_id=${client_id}&redirect_uri=${redirect_uri}&scope=email,pages_messaging `

    let buttonLogin = document.getElementById('btn-login')
    buttonLogin.addEventListener('click', function (e) {
        window.location.href = url;
    })
}