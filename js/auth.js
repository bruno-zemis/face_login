class Auth {
    constructor (httpGet) {
        this.http = httpGet
        this.client_id = 472533196581475
        this.client_secret = '014b45307f5dee5a658fb11a6638f230'
        this.redirect_uri = 'https://localhost:8080/login'
    }
    getAccessToken (code) {
        const url = "oauth/access_token"
        const params = {
            client_id: this.client_id,
            client_secret: this.client_secret,
            redirect_uri: this.redirect_uri,
            code: code
        }
        return this.http(url,{params})
    }
}
export default Auth