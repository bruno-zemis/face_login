const host = 'https://graph.facebook.com/v3.1/'

export default function http (url, config) {
    const params = config.params || {}
    delete config.params

    config = Object.assign(config)

    url = Object.keys(params).reduce((acumuladora, atual, i) => {
        let sufix = (i ? '&': '?') + `${atual}=${params[atual]}`
        return `${acumuladora}${sufix}`
    }, url)

    return fetch(host + url,Object.assign(config, {method: 'GET',})).then(res => res.json())
}
