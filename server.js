let express  = require("express")
let fs = require("fs")
let bodyParser = require('body-parser')

const app = express()
const port = 3000

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false}))
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.post("/user", function (req, res){
    let line = []
    line.push(req.body.first_name)
    line.push(req.body.last_name)
    line.push(req.body.email)
    fs.writeFile(__dirname + '/csv/lead_game.csv', line.join(',') + '\n', 'utf-8', function (err) {
        if (err) res.sendStatus(500);
        res.sendStatus(201)
    });
})

app.listen(port, () => {
    console.log(`Servidor Iniciado na porta => ${port}`)
})